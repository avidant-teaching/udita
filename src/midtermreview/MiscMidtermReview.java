public class MiscMidtermReview {

    
    /*

        a -> {1, 3, 5, 7}
        b -> {2, 4, 6, 8, 10, 12, 14, 16, 18}
        c -> {1, 2, 3, 4, 5, 6, 7, 8}


            // i  |  n 
            //----|---- 
            // 0  |  0  
            // 1  |  2  
            // 2  |  4  
            // ....
            n = 2 * i


        c[8] = b[4]
        c[9] = b[5]
        c[10] = b[6]

        difference of 4




            // i  |  n 
            //----|---- 
            // 0  |  10  
            // 1  |  12  
            // 2  |  14  
            // ....
            n = (2 * i) + 10




            // i  |  n 
            //----|---- 
            // 0  |  1  
            // 1  |  3  
            // 2  |  5  
            // ....
            n = (2 * i) + 1



            // i  |  n 
            //----|---- 
            // 1  |  1  
            // 2  |  3  
            // 3  |  5  
            // ....
            n = (2 * i) - 
    1


    */

    public static int[] interleave(int[] a, int[] b) {
        
        int[] result = new int[a.length + b.length];

        int smallerLength;
        if (a.length < b.length) {
            smallerLength = a.length;
        } else {
            smallerLength = b.length;
        }

        for (int i = 0; i < smallerLength; i++) {
            result[2 * i] = a[i];
            result[(2 * i) + 1] = b[i];
        }

        if (a.length > smallerLength || b.length > smallerLength) {
            // we need to add more elements to the result
            int[] largerArray;
            if (a.length > smallerLength) {
                largerArray = a;
            } else {
                largerArray = b;
            }
            for (int i = smallerLength * 2; i < result.length; i++) {
                result[i] = largerArray[i - smallerLength];
            }
        }
        return result;
    }

    public static int findSecondHighest(int[] a) {

        if (a.length < 2) {
            return -1;
        }

        int indexOfLargestValue, indexOfSecondLargestValue;
        if (a[0] > a[1]) {
            indexOfLargestValue = 0;
            indexOfSecondLargestValue = 1;
        } else {
            indexOfLargestValue = 1;
            indexOfSecondLargestValue = 0;
        }

        for (int i = 2; i < a.length; i++) {
            if (a[i] > a[indexOfLargestValue]) {
                // second value is at indexOfLargestValue
                // largest value is at index i

                // we have 3 values -> i,  indexOfLargestValue, indexOfSecondLargestValue
                // which value do we not care about -> indexOfSecondLargestValue since it is now the index of the 3rd largest value. So overwrite this first
                indexOfSecondLargestValue = indexOfLargestValue;
                indexOfLargestValue = i;
            }
        }

        return indexOfSecondLargestValue;
    }

    public static int getSum(int[] array) {
        int sum = 0;
        for (int element : array) {
            sum += element;
        }
        return sum;
    }

    public static int[] getSums1(int[][] array) {
        int[] sums = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            sums[i] = getSum(array[i]);
        }
        return sums;
    }

    public static int[] getSums2(int[][] array) {
        int[] result = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            result[i] = getSum(array[i]);
        }
        return result;

    }

}
