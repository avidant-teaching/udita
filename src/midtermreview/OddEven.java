import java.util.Random;

public class OddEven {

	public static void main(String[] args) {
		Random random = new Random();
		int[] values = new int[random.nextInt()];
		for (int i = 0; i < values.length; i++) {
			values[i] = random.nextInt(101);
		}

		int[] oddArray = oddNumArray(values);
		int[] evenArray = evenNumArray(values);
		
	}

	public static int[] oddNumArray(int[] values) {
		int oddCount = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] % 2 == 1) {
				evenCount++;
			}
		}

		int[] result = new int[oddCount];
		int resultIndex = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] % 2 == 1) {
				result[resultIndex] = values[i];
				resultIndex++;
			}
		}

	}

	public static int[] evenNumArray(int[] values) {
		int eventCount = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] % 2 == 0) {
				eventCount++;
			}
		}

		int[] result = new int[evenCountCount];
		int resultIndex = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i] % 2 == 0) {
				result[resultIndex] = values[i];
				resultIndex++;
			}
		}

	}	
}
