public class ForEachLoops {

    public static void forEachInt(int[] array) {
        for (int element : array) {
            System.out.println(element); // not the index, it'll be the value at the index
        }
    }

    public static void forEachString(String[] array) {
        for (String element : array) {
            System.out.println(element); // not the index, it'll be the value at the index
        }
    }

    public static void forEachMultiDimInt(int[][] array) {
        /*
            array -> {{1, 2, 3}, {4, 5, 6}, {5, 5, 5}}
         */


        for (int[] innerArray : array) {
            // innerArray is the inner array and not the index
            // {1, 2, 3}
            // {4, 5, 6}
            // {5, 5, 5}
            for (int element : innerArray) {
                System.out.println(element); // not the index, it'll be the value at the index
            }
        }
    }

    public static void forEachMultiDimString(String[][] array) {
        for (String[] innerArray : array) {
            for (String element : innerArray) {
                System.out.println(element);
            }
        }
    }

}
