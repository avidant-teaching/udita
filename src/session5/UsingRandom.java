package session5;

import java.util.Random;

public class UsingRandom {

    public static void main(String[] args) {
        Random randomObject = new Random();

        randomObject.nextInt(1000); // 0 to 999 (inclusive)

        int i = randomObject.nextInt(21) - 10;  // -10 to 10 (inclusive)
    }
}
