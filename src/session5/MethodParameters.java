package session5;

public class MethodParameters {

    public static void main3() {
        int x = 3, y = 2, z = 5;
        x = method3(x, y, z);
        // x = 19, y = 2, z = 5
    }

    public static void main3AndAHalf() {
        int x = 3, y = 2, z = 5;
        int b;
        int a = method3(x, y, z);
        // a = 19, b = we don't know yet, x = 3, y = 2, z = 5
    }


    public static int method3(int x, int y, int z) {
        // x = 3, y = 2, z = 5
        int a = x;          // a = 3
        int b = y;          // b = 2
        int c = z;          // c = 5
        a++;                // a = 4, b = 2, c = 5, x = 3, y = 2, z = 5
        b++;                // a = 4, b = 3, c = 5, x = 3, y = 2, z = 5
        z++;                // a = 4, b = 3, c = 5, x = 3, y = 2, z = 6
        c = x * y++;        // a = 4, b = 3, c = 6, x = 3, y = 3, z = 6
        b = ++x;            // a = 4, b = 4, c = 5, x = 3, y = 3, z = 6
        return a + b + c + z;       // 19
    }

    public static void main2() {
        int x = 3;
        int y = 7;
        x = method2(x, y);
        // x = 7 (from the method), y = 7
    }

    public static int method2(int x, int y) {
        // x = 3, y = 7
        x = 7 * y;      // x = 49, y = 7
        x++;            // x = 50, y = 7
        return y;       // 7
    }

    public static void main1() {
        int x = 7;
        int y = 9;
        method1(y, x);
    }

    public static void method1(int x, int y) {
        // x = 9, y = 7
        System.out.println(x);      // 9
        System.out.println(y);      // 7
        x = x + y;                  // x = 16, y = 7
        x  += y;                    // x = 23, y = 7
        System.out.println(x);      // 23
        System.out.println(y);      // 7
        y = x;                      // y = 23, x = 23
        System.out.println(x);      // 23
        System.out.println(y);      // 23
        x++;                        // x = 24, y = 23
        System.out.println(x);      // 24
        System.out.println(y);      // 23
    }
}
