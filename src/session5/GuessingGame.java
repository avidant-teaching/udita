package session5;

import java.util.Random;
import java.util.Scanner;

public class GuessingGame {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random randomObject = new Random();

        playGame(scanner, randomObject);
    }

    public static void playGame(Scanner scanner, Random randomObject) {
        do {
            int randomNumber = getRandomNumber(randomObject);
            int userInput = getUserInput(scanner);
            while (userInput != randomNumber) {
                provideUserHint(userInput, randomNumber);
                userInput = getUserInput(scanner);
            }
            System.out.println("You guessed right");
            System.out.println("Do you want to play again?");
        } while (scanner.next().equalsIgnoreCase("yes"));  // STOP CONDITION: user input is not yes

    }

    public static int getRandomNumber(Random randomObject) {
        return randomObject.nextInt(100) + 1;
    }

    public static int getUserInput(Scanner scanner) {
        System.out.println("Enter a number");
        return scanner.nextInt();
    }

    public static void provideUserHint(int userInput, int randomNumber) {
        if (userInput > randomNumber) {
            System.out.println("You are higher");   // hint
        } else if (userInput < randomNumber) {
            System.out.println("You are lower");    // hint
        }
    }

    public static void withoutMethods() {
        Scanner scanner = new Scanner(System.in);
        Random randomObject = new Random();

        // while the user wants to play:
            // get a random number
            // while the user does not guess correctly:
                // ask the user to guess
                // give them a hunt
            // once the user guesses correctly:
                // tell them they are correct


        // while the user wants to play
        // the loop has to run atleast once, so yaay do while loops for giving us that functionality
        do {
            // get a random number
            int randomNumber = randomObject.nextInt(100) + 1;

            // while the user does not guess correctly:
            System.out.println("Enter a number");
            int userInput = scanner.nextInt();

            while (userInput != randomNumber) {
                // ask the user to guess
                // give them a hint
                if (userInput > randomNumber) {
                    System.out.println("You are higher");   // hint
                } else if (userInput < randomNumber) {
                    System.out.println("You are lower");    // hint
                }

                System.out.println("Enter a number");
                userInput = scanner.nextInt();

            }

            // the user is correct
            // once the user guesses correctly:
            // tell them they are correct

            System.out.println("You guessed right");

            System.out.println("Do you want to play again?");
        } while (!scanner.next().equals("no"));  // STOP CONDITION: they type no (user input is "no")
//        } while (scanner.next().equalsIgnoreCase("yes"));  // STOP CONDITION: user input is not yes


    }

//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        Random randomObject = new Random();
//
//        int randomNum = randomObject.nextInt(100) + 1;
//        System.out.println("Enter a number");
//        int userInput = scanner.nextInt();
//
//        if (userInput > randomNum) {
//            System.out.println("You are higher");
//        } else if (userInput < randomNum) {
//            System.out.println("You are lower");
//        } else {
//            System.out.println("You guessed right!");
//        }
//    }
}
