import java.util.Random;
import java.util.Scanner;

public class GuessingGame {

    public static void main(String[] args) {
        // Setup the Scanner and Random
        // setup loop variable for new games (changes when user says they don't want to play again)
        // while user wants to play:
            // Pick a random number
            // ask user to guess
            // set count to 1
            // check if answer is correct and print hints if needed or number of guesses used
            // while user guess != random number:
                // ask user to guess
                // increment count
                // check if answer is correct and print hints if needed or number of guesses used
            // ask if the user wants to play again
            // if user says no, switch the loop variable

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        boolean continuePlaying = true;
        while(continuePlaying) {
            int randomNumber = getRandomNumber(random);
            int userGuess = getUserGuess(scanner);
            int count = 1;
            printHintOrResult(userGuess, randomNumber, count);
            while (userGuess != randomNumber) {
                userGuess = getUserGuess(scanner);
                count++;
                printHintOrResult(userGuess, randomNumber, count);
            }
            System.out.print("Do you want to play again? ");
            String playAgain = scanner.next();
            if (!playAgain.equalsIgnoreCase("y") && !playAgain.equalsIgnoreCase("yes")) {
                continuePlaying = false;
            }
        }
    }

    public static void otherWay() {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        boolean continuePlaying = true;
        while(continuePlaying) {
            int randomNumber = getRandomNumber(random);
            int count = 0;
            int userGuess = -1;
            while (userGuess != randomNumber) {
                userGuess = getUserGuess(scanner);
                count++;
                printHintOrResult(userGuess, randomNumber, count);
            }
            System.out.print("Do you want to play again? ");
            String playAgain = scanner.next();
            if (!playAgain.equalsIgnoreCase("y") && !playAgain.equalsIgnoreCase("yes")) {
                continuePlaying = false;
            }
        }
    }

    public static int getRandomNumber(Random random) {
        System.out.println("I'm thinking of a number between 1 and 100...");
        return random.nextInt(100) + 1;
    }

    public static int getUserGuess(Scanner scanner) {
        System.out.print("Your guess? ");
        return scanner.nextInt();
    }

    public static void printHintOrResult(int userGuess, int randomNumber, int guessCount) {
        if (userGuess > randomNumber) {
            System.out.println("It's lower");
        } else if (userGuess < randomNumber) {
            System.out.println("It's higher");
        } else {
            System.out.println("You got it right in " + guessCount + " guesses");
        }
    }

}


/*
I'm thinking of a number between 1 and 100...
Your guess? 50
It's lower.
Your guess? 25
It's lower.
Your guess? 12
It's lower.
Your guess? 6
You got it right in 4 guesses
Do you want to play again? y

I'm thinking of a number between 1 and 100...
Your guess? 50
It's lower.
Your guess? 25
It's lower.
Your guess? 12
It's higher.
Your guess? 18
You got it right in 4 guesses
Do you want to play again? YES
 */