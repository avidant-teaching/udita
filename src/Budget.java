import java.util.Scanner;

public class Budget{

    public static void main (String [] args){
        Scanner scanner = new Scanner(System.in);
        double totalIncome = incomeAmount (scanner);

        System.out.println ("Enter 1) monthly or 2) daily expenses?");
        int expensesType = scanner.nextInt();
        double totalExpenses = expensesAmount (scanner);

        // Instead I'd have used only 1 method: monthlyExpenses
        /*
        if (expensesType == 1) {    // monthlyExpenses
            monthlyExpenses (totalExpenses, totalIncome);
        } else {
           // since totalExpenses is per day, just convert it to monthly
           monthlyExpenses(totalExpenses * 30, totalIncome);
        }
         */

        if (expensesType == 1){
            monthlyExpenses (totalExpenses, totalIncome);
        } else {
            dailyExpenses (totalExpenses, totalIncome);
        }

        if (totalExpenses > totalIncome){
            double excessSpending = totalExpenses - totalIncome;
            // -1 for the space after `/n`
            System.out.println ("You spent $ " + excessSpending + "more than you earned this month. \n You're a big spender");
        } else {
            double excessSaving = totalIncome - totalExpenses;
            // -1 for the space after `/n`
            System.out.println ("You earned $ " + excessSaving + "more than you spent this month. \n You're a big saver.");
        }

    }

    public static double incomeAmount (Scanner scanner){
        System.out.println("How many categories of income?");
        int incomeCategories = scanner.nextInt();
        double totalIncomeInput = 0;
        for (int i = 0; i < incomeCategories ; i++){
            // -0 use `\t` instead for the spacing in the print
            // Use .print since we want the input on the same line
            // System.out.print("\tNext income amount? $");
            System.out.println("  Next income amount?");
            double incomeInput = scanner.nextDouble();
            totalIncomeInput += incomeInput;
        }
        return totalIncomeInput;
    }

    public static double expensesAmount (Scanner scanner){
        System.out.println("How many categories of expense?");
        int expensesCategories = scanner.nextInt();
        double totalExpensesInput = 0;
        for (int j = 0; j < expensesCategories ; j++){
            // -0 use `\t` instead for the spacing in the print
            // Use .print since we want the input on the same line
            // System.out.print("\tNext income amount? $");
            System.out.println("	Next expense amount?");
            double expensesInput = scanner.nextDouble();
            totalExpensesInput += expensesInput;
        }
        return totalExpensesInput;
    }

    public static void monthlyExpenses (double expenses, double income) {
        double incomePerDay = income / 30 ;
        double expensesPerDay = expenses / 30;
        System.out.println("Total income = $" + income + " ($" + incomePerDay + "/day)");
        System.out.println("Total expenses = $" + expenses + " ($" + expensesPerDay + "/day)");
    }

    public static void dailyExpenses (double expensesDaily, double incomeDaily) {
        // income is always monthly. So this would be wrong
        double incomePerMonth = incomeDaily * 30;
        double expensesPerMonth = expensesDaily * 30;
        System.out.println("Total income = $" + incomePerMonth + " ($" + incomeDaily + "/day)");
        System.out.println("Total expenses = $" + expensesPerMonth + " ($" + expensesDaily + "/day)");
    }


    public static double formatMoney(double unformattedAmount) {
        // ????.xyzabc -> ???xy
        int tempVariable = (int) (unformattedAmount * 100);
        // ???xy -> ???.xy
        return tempVariable / 100.0;
    }



}
  
  /*
  
  // main method - call incomeAmount method
    // print statmenet for categories of income 
    // store the number in n 
    // use for loop with print statement next income amount 
    // return total income amount 
  
  // main methpd ask 1 or 2 
  // main method - call expensesAmount method 
    // print statement for categories of expenses 
    // store number in n 
    // for loop
    // return expenses amount
  
  // main method - use if statements for 1 or 2 
    // call methods accordingly 
  // method 1 - monthlyExpenses
    // print expenses amount (return of expensesAmount method)
    // divide by 30 for brackets
  // method 2 - dailyExpenses
    // multiply return of expensesAmount method by 30 
    // print the amount & also the divided by 30 amount ()
  // main method subtract total income & expenses 
    // use if else statements to print saver / spender 
   

  // get incomeCategoryCount from user as int
  // set totalIncome to 0
  // for int 0 to incomeCategoryCount:
    // get next income amount and add it to totalIncome
  // Ask user if expenses are monthly or daily
  // get expenseCategoryCount from user as int
  // set totalExpenses to 0
  // for int 0 to expenseCategoryCount
    // get next expense and add it to totalExpenses
  // if expenses are monthly:
      // Print total income and expenses (monthly and daily (divide by 30))
  // else:
    // print total income (monthly and daily (divide by 30))
    // print total expenses (monthly (multiple by 30) and daily (divide by 30))
  // if expenses > income:
    // print spender message
  // else:
    // print saver message
  
  
  
  -------------
  How many categories of income? 3
    Next income amount? $1000
    Next income amount? $250.25
	  Next income amount? $175.50

Enter 1) monthly or 2) daily expenses? 1
How many categories of expense? 4
	Next expense amount? $850
	Next expense amount? $49.95
	Next expense amount? $75
	Next expense amount? $120.67

Total income = $1425.75 ($45.99/day)
Total expenses = $1095.62 ($35.34/day)

You earned $330.13 more than you spent this month.
You're a big saver.
----
You spent $403.93 more than you earned this month.
You're a big spender
---
*/


  