package session2;

public class Stars {

    public static void main(String[] args) {

        /*
                *********
                 *******
                  *****
                   ***
                    *
                   ***
                  *****
                 *******
                *********
         */

        step3();
    }

    public static void step1() {
        /*

         *********
         *******
         *****
         ***
         *

         */

        for (int i = 5; i > 0; i--) {
            for (int n = 0; n < 2 * i - 1; n++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void step2() {

        /*

         *********
          *******
           *****
            ***
             *

         */

        /*
            i  |  blanks
           ----|---------
            5  | 0
            4  | 1
            3  | 2
            2  | 3
            1  | 4

            5 - i
         */

        for (int i = 5; i > 0; i--) {

            for (int n = 0; n < 5 - i; n++) {
                System.out.print(" ");
            }

            for (int n = 0; n < 2 * i - 1; n++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void step3() {
        // TOP
        for (int i = 5; i > 0; i--) {
            for (int n = 0; n < 5 - i; n++) {
                System.out.print(" ");
            }
            for (int n = 0; n < 2 * i - 1; n++) {
                System.out.print("*");
            }
            System.out.println();
        }

        /*
                ***
               *****
              *******
             *********
         */

        for (int i = 0; i < 4; i++) {

            // SPACES
            // i = 0; 3
            // i = 1; 2
            // i = 2; 1
            // i = 3; 0
            // 3 - i
            for (int n = 0; n < 3 - i; n++) {
                System.out.print(" ");
            }

            // STARS
            // i = 0, 3
            // i = 1, 5
            // i = 2, 7
            // i = 3, 9
            // 2 * i + 3
            for (int n = 0; n < 2 * i + 3; n++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void usingForLoops() {
        for (int i = 5; i > 0; i--) {
            for (int j = 0; j < 5 - i; j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < 2 * i - 1; k++) {
                System.out.print("*");
            }
            System.out.println();
        }

        for (int l = 0; l < 4; l++) {
            for (int m = 0; m < 3 - l; m++) {
                System.out.print(" ");
            }
            for (int n = 0; n < 2 * l + 3; n++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void convertForToWhile() {
        for (int k = 0; k < 10; k++) {
            System.out.println(k);
        }

        // STEP 1: declare k as an int and initialize it to 0
        // STEP 2: check if k < 10
        // STEP 3: if true, enter loop. if false, exit loop
        // STEP 4: do the loop
        // STEP 5: increment the value of k
        // STEP 6: jump to step 2

        // Step 1: declare k as an int and initialize it to 0
        int k = 0;

        while (k < 10) {
            System.out.println(k);
            k++;
        }
    }
}
