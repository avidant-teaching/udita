import java.util.Random;
import java.util.Scanner;

public class Cse4Linear {
	
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int[] userInput = getUserInput();
		// Binary search

		scramble(userInput);
		// Linear Search

	}

	public static void scramble(int[] array) {
		Random random = new Random();
		for (int i = 0; i < array.length; i++) {
			int randomIndex = random.nextInt(array.length);

			int originalNumber = array[i];
			int randomNumber = array[randomIndex];

			array[i] = randomNumber;
			array[randomIndex] = originalNumber;
		}
	}

	public static int[] scramble(int[] array) {
		Random random = new Random();
		int[] result = new int[15];
		for (int i = 0; i < array.length; i++) {
			int randomIndex = random.nextInt(array.length);

			int originalNumber = array[i];
			int randomNumber = array[randomIndex];

			result[i] = randomNumber;
			result[randomIndex] = originalNumber;
		}
		return result;
	}	

	public static int[] userInput(Scanner scan) {
		int[] result = new int[15];
		result[0] = getIntegerFromUser(scan);
		for (int i = 1; i < 15; i++) {
			int nextInt = getIntegerFromUser(scan);
			int lastInt = result[i - 1];
			while (lastInt > nextInt) {
				System.out.println("number needs to be greater than lastInt");
				nextInt = getIntegerFromUser(scan);
			}
			result[i] = nextInt;
		}
		return result;
	}

	public static int getIntegerFromUser(Scanner scan) {
		while (true) {
			System.out.println("Enter a number");
			if (scan.hasNextInt()) {
				// has a number
				int number = scan.nextInt();
				if (number >= 0 && number <= 100) {
					return number;
				}
				System.out.println("between 0 and 100");
			} else if (scan.hasNext()) {
				// not a number
				System.out.println("enter a number, not a string");
				scan.next();
			}
		}
	}

}
