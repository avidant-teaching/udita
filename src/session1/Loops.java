package session1;

import java.util.Scanner;

public class Loops {

    public static void main(String[] args) {
        homework();
    }

    public static void homework() {
        /*

         *
         ***
         *****
         *******
         *********

         */

        /*

              row   |   count
              (n)   |
           ---------|------------
               0    |     1
               1    |     3
               2    |     5
               3    |     7

               2n -> it increases by 2 every time
               +1 -> just to get it set to the right first value

              row   |   count
              (n)   |
           ---------|------------
               1    |     1
               2    |     3
               3    |     5
               4    |     7

               2n +/- ?
               2n - 1

         */

        for (int n = 0; n < 5; n++) {
            for (int i = 0; i < 2 * n + 1; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void letsEnjoyLoops() {
        /*
            0
            01
            012
            0123
            ...
            0123456789
         */

//        System.out.println("0\n01\n012\n0123\n01234\n012345\n0123456\n01234567\n012345678\n0123456789");

        // line 0: all numbers from 0 to 0 in one line, then prints a new line
        // line 1: all numbers from 0 to 1 in one line, then prints a new line
        // line 2: all numbers from 0 to 2 in one line, then prints a new line
        // ...
        // line 9: all numbers from 0 to 1 in one line, then prints a new line

        // line n: all numbers from 0 to n in one line, then print a new line

//        for every number n from 0 to 9,
//            print all numbers from 0 to n in one line, then print a new line

        for (int n = 0; n <= 9; n++) {
            // Step 1: print all the numbers from 0 to n on one line
            // This loop goes over every number from 0 to n
            for (int i = 0; i <= n; i++) {
                System.out.print(i);
            }
            // Step 2: print a new line
            System.out.println();
        }

        /*
        Trace the loop:

        n = 0; true:
            i = 0; true; print 0
            i = 1; false
            print the new line
        n = 1; true
            i = 0; true; print 0
            i = 1; true; print 1
            i = 2; false
            print the new line
        n = 2; true
            i = 0; true
            ...
        ...
        n = 9; true:
            i = 0; true; print 0
            i = 1; true; print 1
            ...
            i = 8; true; print 8
            i = 9; true; print 9
            i = 10; false
            print a new line
        n = 10; false
         */

    }

    public static void doWhileLoops() {
        // The loop is run at least once
        // This works great for the example used for whileLoops because we want atleast one number

        // write a program that asks the user to input *all* their favorite numbers. Then print the average of those numbers
        // We want the user to provide atleast 1 number
        System.out.println("Please enter all your favorite numbers. Then type S when you have entered them all");
        Scanner scanner = new Scanner(System.in);

        // scanner has a method scanner.hasNextInt() which returns true if there is an int available to read
        // scanner has a method scanner.nextInt() which reads the next method

        double total = 0;
        int count = 0;

        do {
            int nextNum = scanner.nextInt();
            total += nextNum;
            count++;
        } while (scanner.hasNextInt());

        System.out.println("The average is: " + (total / count));
    }

    public static void doWhileVsWhile() {
//        do {
//            int nextNum = scanner.nextInt();
//            total += nextNum;
//            count++;
//        } while (scanner.hasNextInt());

        // IS THE SAME AS

//        int someNumber =  scanner.nextInt();
//        total += someNumber;
//        count++;
//        while (scanner.hasNextInt()) {
//            int nextNum = scanner.nextInt();
//            total += nextNum;
//            count++;
//        }

        // Do while is the same as a while loop with extra work done before the loops starts
    }

    public static void whileLoops() {
        // for loops vs while loops
        //  - for: know the end point / it's a fixed range / you know when exactly to stop
        //  - while loop: otherwise

        // write a program that asks the user to input *all* their favorite numbers. Then print the average of those numbers
        System.out.println("Please enter all your favorite numbers. Then type S when you have entered them all");
        Scanner scanner = new Scanner(System.in);

        // scanner has a method scanner.hasNextInt() which returns true if there is an int available to read
        // scanner has a method scanner.nextInt() which reads the next method

        double total = 0;
        int count = 0;

        // while loop goes here
        // At what condition do we stop: when scanner has no more ints to read. scanner.hasNextInt() is false
        // The loop condition is the negation of the stop condition
        while (scanner.hasNextInt()) {
            int nextNum = scanner.nextInt();
            total += nextNum;
            count++;
        }

        System.out.println("The average is: " + (total / count));
    }

    public static void complexForLoops() {
        // print all even numbers from 1 to 10
        for (int i = 2; i <= 10; i += 2) {
            System.out.println(i);
        }

        // print all multiples of 5 from 1 to 100
        for (int i = 5; i <= 100; i += 5) {
            System.out.println(i);
        }

        // print all even multiples of 5 from 1 to 100
        for (int i = 5; i <= 100; i += 5) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
        // another way to do it
        for (int i = 10; i <= 100; i += 10) {
            System.out.println(i);
        }
    }

    public static void forLoops() {
        // print every number from 1 to 10
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }

        // print all odd numbers from 1 to 10
        for (int i = 1; i <= 10; i += 2) {
            System.out.println(i);
        }
    }
}
