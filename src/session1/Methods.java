package session1;

public class Methods {
    public static void main(String[] args) {
        double originalAmount = 22. / 7;

        // java -> formatMoney with input originalAmount
        // formatMoney would return the value 3.14
        // double formattedMoney = 3.14;
        double formattedMoney = formatMoney(originalAmount);

        System.out.println(originalAmount);
        System.out.println(formattedMoney);

        double a = 3.12334, b = 4354.2342, c = 3453234.234235, d = 234345.34534;
        System.out.println(formatMoney(a));
        System.out.println(formatMoney(b));
        System.out.println(formatMoney(c));
        System.out.println(formatMoney(d));
    }

    public static double formatMoney(double unformattedAmount) {
        int multipliedByHundred = (int) (unformattedAmount * 100);
        return multipliedByHundred / 100.;
    }

    /*
        public -> anyone can see/use it
        static
        void -> there is no output
        main -> name of the method
        String[] args -> the input (it is a variable with type String[] called args)
     */

    // create a method called formatMoney that takes in a double as input and returns the same value with only 2 decimals points
    /*
        input: double unformattedAmount
        name: formatMoney
        output: double
     */
//    public static double formatMoney(double unformattedAmount) {
//        int multipliedByHundred = (int) (unformattedAmount * 100);
//        double formattedAmount = multipliedByHundred / 100.;
//        return formattedAmount;
//    }

    // create a method called printWelcome that prints "Welcome to my program"
    public static void printWelcome() {
        System.out.println("Welcome to my program");
    }
}