import java.util.Scanner;

public class CalculateAverage {

    public static void main(String[] args) {

        // Setup the scanner
        // setup sum and count
        // Get input from user
        // while input is positive:
            // add input to sum
            // increment count
            // get input from user
        // print average

        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int count = 0;
        int input = getUserInput(scanner);
        while (input >= 0) {
            sum += input;
            count++;
            input = getUserInput(scanner);
        }
        printAverage(sum, count);

    }

    public static int getUserInput(Scanner scanner) {
        System.out.println("Enter a number");
        return scanner.nextInt();
    }

    public static void printAverage(int sum, int count) {
        System.out.println("The average is " + (sum * 1.0 / count));
    }
}