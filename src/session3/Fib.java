// CSE003 Udita Agarwal
// 23rd October 2020
// prints fibonacci sequence 

package session3;

import java.util.Scanner; // to use the scanner class

public class Fib {

    public static void main (String args []){

        Scanner scanner = new Scanner (System.in);

        System.out.println("Enter the first number in the sequence: ");
        int firstInput = getNextIntFromUser(scanner);

        System.out.println("Enter the second number in the sequence: ");
        int secondInput = getNextIntFromUser(scanner);

        System.out.println("How many custom Fibonacci numbers should be printed? ");
        int numbersNeedToBePrinted = getNextIntFromUser(scanner);

        System.out.println("FOR LOOP");
        fibUsingForLoop(firstInput, secondInput, numbersNeedToBePrinted);

        System.out.println("WHILE LOOP");
        fibUsingWhileLoop(firstInput, secondInput, numbersNeedToBePrinted);

    }

    // VISIBILITY static RETURN_TYPE/void METHOD_NAME(PARAMETERS)

    public static int getNextIntFromUser(Scanner scanner) {
        while (!scanner.hasNextInt()){
            System.out.println("Sorry, you did not enter an integer. Try again: ");
        }
        return scanner.nextInt();
    }

    public static void fibUsingForLoop(int forLoop1, int forLoop2, int num) {
        System.out.print(forLoop1 + ", " + forLoop2);
        for (int i = 0; i < num - 2; i++) {
            // Look at the previous 2 numbers and print their sum
            int sum = forLoop1 + forLoop2;
            System.out.print(", " + sum);

            // Then update the "previous" 2 numbers to the new "previous"
            forLoop1 = forLoop2;
            forLoop2 = sum;
        }
        System.out.println();
    }

    public static void fibUsingWhileLoop(int a1, int a2, int num) {
        int i = 0;
        System.out.print(a1 + ", " + a2);
        while (i < num - 2) {
            int sum = a1 + a2;
            System.out.print(", " + sum);

            a1 = a2;
            a2 = sum;

            i++;
        }
        System.out.println();
    }
}